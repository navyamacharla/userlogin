export class Post {
    postId: string;
    title: string;
    description: string;
}