import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Post } from '../models/posts';
import { Router } from '@angular/router';
import { ViewpostPage } from '../viewpost/viewpost.page';

@Component({
  selector: 'app-createpost',
  templateUrl: './createpost.page.html',
  styleUrls: ['./createpost.page.scss'],
})
export class CreatepostPage implements OnInit {
  todo = {
    postid: '',
    title: '',
    description: ''
  };
  post: Post;
  constructor(private http: HttpClient,
              private viewpost: ViewpostPage,
              private authService: AuthService, private router: Router) { }

  ngOnInit() {
  }
  createPost(form: NgForm) {
    console.log('Working');
    this.post = {
      postId: form.value.postid,
      title: form.value.title,
      description: form.value.description
    };
    console.log(this.post);
    const token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${token}`});
    this.http.post('http://192.168.5.59:4200/api/users/posts' + '/addPost', this.post, {headers})
    .subscribe((data: any) => {
      alert('Post Created Successfully!');
      console.log(data);
      this.router.navigateByUrl('/dashboard');
    }, error => {
      console.log(error.error.message);
    });
  }
}
