import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { User } from 'src/app/models/users';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {NgForm} from '@angular/forms';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  user: User;
  edit = false;
  constructor(
              private authservice: AuthService,
              private http: HttpClient,
              private popOverCtrl: PopoverController) { }

  ngOnInit() {
  }
  ionViewWillEnter() {
    this.authservice.user().subscribe(
      user => {
        console.log(user);
        this.user = user;
      }
    );
  }
  editProfile() {
    this.edit = true;
  }
  isSave(form: NgForm) {
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    const body = {
      username : form.value.username,
      firstName : form.value.fName,
      lastName : form.value.lName
    };
    this.http.put('http://192.168.5.59:4200/api/users' + '/updateProfile', body, {headers})
    .subscribe((data: any) => {
      console.log(data);
      alert(data.message);
      this.edit = false;
  }, error => {
    console.log(error.error.message);
    alert(error.error.message);
  });
  }
  // openMenu(myEvent) {
  //  const popover = this.popOverCtrl.create();
  // }
}
