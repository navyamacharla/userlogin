import { Component, OnInit, NgModule } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { ModalController, ToastController } from '@ionic/angular';
import { User } from 'src/app/models/users';
import { CommonModule } from '@angular/common';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { Router } from '@angular/router';
import { NavextraserviceService } from '../services/navextraservice.service';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
@NgModule({
  imports: [CommonModule]
})
export class DashboardPage implements OnInit {
  user: User;
  array = [];
  dropview: any;
  constructor( private authService: AuthService,
               private modalController: ModalController,
               private http: HttpClient,
               private router: Router,
               private navextraservice: NavextraserviceService,
               private toastController: ToastController) {
               }
  ngOnInit() {
    this.navextraservice.type = false;
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/viewPosts', { headers })
    .subscribe((data: any) => {
        // console.log(data[0]);
        this.array = data;
        console.log(this.array);
    }, error => {
      console.log(error.error.message);

    });
  }
  ionViewWillEnter() {
    this.authService.user().subscribe(
      user => {
        console.log(user);
        this.user = user;
      }
    );
  }
  dismissLogout() {
    this.modalController.dismiss();
  }
  async logout() {
    this.authService.logout();
    const toast = await this.toastController.create({
      message: 'Logout Successful',
      duration: 2000,
      showCloseButton: true
    });
    toast.present();
  }
  doRefresh(event) {
    console.log('Refreshing...!');

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    if (this.dropview === 'myposts') {
      this.navextraservice.type = true;
      return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/viewMyPosts', { headers })
      .subscribe((data: any) => {
          console.log(data[0]);
          this.array = data;
          console.log(this.array);
      }, error => {
        console.log(error.error.message);
      });
    } else {
        this.navextraservice.type = false;
        return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/viewPosts', { headers })
        .subscribe((data: any) => {
            console.log(data[0]);
            this.array = data;
            console.log(this.array);
        }, error => {
          console.log(error.error.message);

        });
    }
  }
  viewPost(postId: string) {
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/getPost/' + postId, { headers })
    .subscribe((data: any) => {
      console.log(data);
      this.navextraservice.setExtras(data);
      this.router.navigateByUrl('/viewpost');

  }, error => {
    console.log(error.error.message);

  });
  }
  myPosts() {
    console.log(this.dropview);
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    if (this.dropview === 'myposts') {
          this.navextraservice.type = true;
          console.log('Null');
          return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/viewMyPosts', { headers })
          .subscribe((data: any) => {
              this.array = data;
          }, error => {
            console.log(error.error.message);
          });
    } else {
          this.navextraservice.type = false;
          return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/viewPosts', { headers })
          .subscribe((data: any) => {
            this.array = data;
            console.log(data);
        }, error => {
          console.log(error.error.message);

        });
    }
  }
  deletePost(postId: string) {
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    return this.http.delete('http://192.168.5.59:4200/api/users/posts' + '/deletePost/' + postId, { headers })
    .subscribe(async (data: any) => {
      console.log(data);
      const toast = await this.toastController.create({
          message: 'Post deleted',
          duration: 2000,
          showCloseButton: true
        });
      toast.present();
  }, error => {
    console.log(error.error.message);
  });
  }
}
