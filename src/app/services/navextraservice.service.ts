import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavextraserviceService {
  extras: any;
  type: boolean;
  constructor() { }
  public setExtras(data) {
    this.extras = data;
  }
  public getExtras() {
    return this.extras;
  }

  public getType() {
    return this.type;
  }
}
