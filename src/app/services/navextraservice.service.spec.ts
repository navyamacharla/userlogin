import { TestBed } from '@angular/core/testing';

import { NavextraserviceService } from './navextraservice.service';

describe('NavextraserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NavextraserviceService = TestBed.get(NavextraserviceService);
    expect(service).toBeTruthy();
  });
});
