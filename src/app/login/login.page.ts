import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';
import { NgForm } from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { RegisterPage } from '../register/register.page';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
passwordshown: boolean;
  constructor(private authService: AuthService,
              private router: Router,
              private toastController: ToastController
    ) {
      this.passwordshown = false;
    }

  ngOnInit() {

  }
 async login(form: NgForm) {
    this.authService.login(form.value.username, form.value.password).subscribe(
      data => {
      },
      error => {
        console.log(error.error.message);
        alert(error.error.message);
      },
      async () => {
        const toast = await this.toastController.create({
          message: 'Login Successful',
          duration: 2000,
          showCloseButton: true
        });
        toast.present();
        this.router.navigateByUrl('/dashboard');
      }
    );
  }
  show() {
  this.passwordshown = !this.passwordshown;
  }
}
