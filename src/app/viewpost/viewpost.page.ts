import { Component, OnInit } from '@angular/core';
import { HttpHeaders , HttpClient} from '@angular/common/http';
import { Router, ActivatedRoute } from '@angular/router';
import { Injectable } from '@angular/core';
import { NavextraserviceService } from '../services/navextraservice.service';
import { NgForm } from '@angular/forms';
import { User } from 'src/app/models/users';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-viewpost',
  templateUrl: './viewpost.page.html',
  styleUrls: ['./viewpost.page.scss'],
})

@Injectable({
  providedIn: 'root'
})
export class ViewpostPage implements OnInit {
details: any;
postType: boolean;
user: User;
id: any;
// time: string;
// date: string;
  constructor(private http: HttpClient, private router: Router,
              private authService: AuthService,
              private activatedRoute: ActivatedRoute,
              private navextraservice: NavextraserviceService) {
                  this.details = navextraservice.getExtras();
                  this.postType = navextraservice.getType();
                  console.log(this.details);
                  // this.date = this.details.createdDate.split('T')[0];
                  // this.time = this.details.createdDate.split('T')[1].split('.')[0];
                  // console.log(this.details.createdDate.toISOString());
               }

  ngOnInit() {
  }
  comment(form: NgForm) {
    console.log(form.value.comm);
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    //  tslint:disable-next-line: max-line-length
    return this.http.post('http://192.168.5.59:4200/api/users/posts/' + this.details._id + '/addComment', { text: form.value.comm}, { headers })
    .subscribe((data: any) => {
      console.log(data);
      alert(data.message);

  }, error => {
    console.log(error.error.message);
  });
  }
  doRefresh(event) {
    console.log('Async operation began');
    console.log(this.navextraservice.type);
    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 1000);
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    console.log(this.details._id);
    return this.http.get('http://192.168.5.59:4200/api/users/posts' + '/getPost/' + this.details._id , { headers })
    .subscribe((data: any) => {
      this.navextraservice.setExtras(data);
      this.details = this.navextraservice.getExtras();
      console.log(this.details);
      // this.date = this.details.comments.commentedOn.split('T')[0];
      // this.time = this.details.comments.commentedOn.split('T')[1].split('.')[0];
    }
    , error => {
      console.log(error.error.message);

    });
  }
  deleteComment(commentId: string) {
    const Token = localStorage.getItem('token');
    const headers = new HttpHeaders({Authorization: `Bearer ${Token}`});
    console.log(this.details._id);
    console.log(commentId);
    // tslint:disable-next-line: max-line-length
    return this.http.delete('http://192.168.5.59:4200/api/users/posts/' + this.details._id + '/deleteComment/' + commentId, { headers })
    .subscribe((data: any) => {
      console.log(data);
      alert(data.message);
  }, error => {
    console.log('Cannot delete');
    console.log(error.error.message);

  });
  }
}
