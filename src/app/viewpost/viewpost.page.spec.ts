import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewpostPage } from './viewpost.page';

describe('ViewpostPage', () => {
  let component: ViewpostPage;
  let fixture: ComponentFixture<ViewpostPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewpostPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewpostPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
