import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController} from '@ionic/angular';
import { LoginPage } from '../login/login.page';
import { NgForm, FormBuilder , Validators, FormControl, FormGroup} from '@angular/forms';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})

export class RegisterPage implements OnInit {
  // validations_form:any;
  // matching_password_group:FormGroup;
  constructor(
               private authService: AuthService,
               private router: Router,
               private toastController: ToastController,
               private formBuilder: FormBuilder) {
      // this.validations_form = this.formBuilder.group({
      //   username: new FormControl('', Validators.required),
      //   firstname: new FormControl('', Validators.required),
      //   lastname: new FormControl('', Validators.required),
      //   password: new FormControl('', Validators.compose([
      //     Validators.minLength(5),
      //     Validators.required,
      //     Validators.pattern('^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])[a-zA-Z0-9]+$')
      //   ])),
      //   confirm_password: new FormControl('', Validators.required)
      // }, (formGroup: FormGroup) => {
      //   if(formGroup.value.password===formGroup.value.confirm_password){
      //     return true;
      //   }else{
      //     return{
      //       areEqual:false
      //     }
      //   }
      // });
    }
    // validation_messages = {
    //   'username': [
    //   // { type: 'pattern', message: 'Username must contain only combination of letters and numbers.' },
    //   { type: 'required', message: 'Username is required.' }
    //   ],
    //   'firstname': [
    //   // { type: 'pattern', message: 'Username must contain atleast one letters and one numbers.' },
    //   { type: 'required', message: 'Username is required.' }
    //   ],
    //   'lastname': [
    //   // { type: 'pattern', message: 'Username must contain only combination of letters and numbers.' },
    //   { type: 'required', message: 'Username is required.' }
    //   ],
    //   'password': [
    //   { type: 'minlength', message: 'Password must be minimum 5 characters.' },
    //   { type: 'required', mes'sage: 'Password is required.' },
    //   { type: 'pattern', message: 'Password must contain combination of upper and lower case letters and numbers.' }
    //   ],
    //   'confirm_password': [
    //   { type: 'required', message: 'Password is required.' },
    //   { type: 'areEqual', message: 'Confirm password is not same.' },
    //   ],

    // }

  ngOnInit() {
  }
  register(form: NgForm) {
    this.authService.register(form.value.fName, form.value.lName, form.value.username, form.value.password).subscribe(
      data  => {
        console.log(data);
        this.router.navigateByUrl('/login');
        },
        error => {
          console.log(error.error.message);
          alert(error.error.message);
        },
        async () => {
          const toast = await this.toastController.create({
            message: 'Registration Successful. Please login',
            duration: 2000,
            showCloseButton: true
          });
          toast.present();
          this.router.navigateByUrl('/login');
        }
    );
      }
}
